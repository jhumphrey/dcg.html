# dcg.html

"Dance card generator" to generate a service schedule for a church, or other group that does things on a monthly and weekly basis.

## Name
DCG2021 Dance Card Generator HTML addition

## Description
Not intended for use in dances. This program actually generates a Worship Service Schedule for a religious orginization. The specific setup is for a Medium sized, American Church of Christ, but could be adapted for any format. It will work "out of the box" for services every Sunday, and monthly things, but would require modification to do Wednesdays or Bi-Annual services.

## Installation
Simply download and open dcg.html in a browser.

## Usage
See the "help" link in the program sidebar.

## Support
File an Issue for support.

## Roadmap
We need to add import functionality to the program. Possibly some way to share lists between multiple people, but that may be too complex.

## Contributing
Feel free to contribute. I will not accept contributions if:
* They change the default behaviour in a way that is contrary to my theology.
* They change the default behaviour in a way I don't like
* I don't like the code, or can't understand it.
* I just don't wanna accept it for other reasons.
If you don't like this, simply fork the project and work on it yourself.
If you are unsure feel free to submit a pull request, explaining what your code does and why.

## Authors and acknowledgment
The name "Dance Card Generator" is in honour of a friend who would alwas tease me about my "dance card" whenever I would orginize a service.
I did most of the code. if you write some code, your name can be placed here if you want.

## License
This is licensed under the MIT/X11 license.

## Project status
This is in semi-active development.

## Forks
This is where I may list forks of this project. Forks are versions of this software with changes by other people. This may be due to another theology, different approach to the project, or just plain grumpiness. No matter, I will list forks here, but I may not list yours for a variety of reasons. For example I will not list Satanist forks, or forks related to the dark side. I will not harmful forks too. You get the idea.

